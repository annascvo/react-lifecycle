import React, { useState } from 'react';
import Input from './Input'
import './Students.css'

function Students() {
    const [arr, setArr] = useState(['Иван', 'Александр', 'Сергей', 'Ирина', 'Елена']);
    const [value, setValue] = useState('');

    function addStudents() {
        setArr([...arr, value]);
        setValue('');
    }

    function remove(index) {
        //setArr([...arr.slice(0, index), ...arr.slice(index + 1)]);
        setArr(arr.filter((el, i, arr) => { return i !== index }));
    }

    return <div className={'containerStudents'}>
        <Input className={'inputName'} value={value} onChange={event => setValue(event.target.value)} />
        <button className={'addButton'} onClick={addStudents}>Добавить</button>
        {arr.map((item, index) =>
            <p className={'nameStudents'} key={index} onClick={() => remove(index)}>{item}</p>
        )}
    </div>;
}

export default Students;
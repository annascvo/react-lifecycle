const Link = ({ url, text, onClick }) => (
        <a href={url} onClick={onClick}>{text}</a>
)

export default Link;
import PropTypes from 'prop-types'

const Button = ({item,onClick}) => {
    return (
        <button onClick={onClick}>{item}</button>
    );
}

Button.propTypes = {
    item: PropTypes.string,
    onClick: PropTypes.func
}

export default Button;
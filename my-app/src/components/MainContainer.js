import { createRef } from 'react';
import Link from './Link';
import IgnoreFirstChild from './IgnoreFirstChild'

function MainContainer() {
    const refOutput = createRef();

    const handleClick = (e) => {
        e.preventDefault()
        console.log(refOutput.current)
    }

    return (
        <div className="container" ref={refOutput}>
            <Link url="#" text="Текст ссылки" onClick={(e) => { handleClick(e) }}></Link>
            <IgnoreFirstChild className="child">
                <div>Первый</div>
                <div>Второй</div>
                <div>Третий</div>
                <div>Четвертый</div>
                <div>Пятый</div>
            </IgnoreFirstChild>
        </div>
    )
}

export default MainContainer;
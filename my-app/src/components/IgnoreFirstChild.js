import React from 'react'

const IgnoreFirstChild = ({ children, className }) => {
    return (
        <div>
            {React.Children.map(children, (child, i) => {
                // игнорируем первого потомка
                if (i < 1) return
                return React.cloneElement(child, {
                    id: `${className}${i}`,
                    style: { color: '#575759' }
                })
            })}
        </div>
    )
}

export default IgnoreFirstChild;
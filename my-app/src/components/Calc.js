/*eslint no-eval: 0 */
import React from 'react';
import './Calc.css'
import store from './constants'
import Button from './Button'


class Calc extends React.Component {
    constructor() {
        super()
        this.state = {
            out: '0'
        }
    }

    tapeNumber(value) {
        if (this.state.out === "0") {
            this.setState({
                out: '',
            });
        }
        this.setState(state => ({
            out: state.out + value,
        }));
    }

    tapeOperation(value) {
        if (value === 'C') {
            this.setState({
                out: '0',
            });
        } else if (value === '=') {
            try {
                this.setState({
                    out: eval(this.state.out),
                });
            } catch {
                this.setState({
                    out: 'Некорректный ввод',
                });
                setTimeout(() => {
                    this.setState({
                        out: '0',
                    });
                }, 1000)
            }
        }
    }

    render() {
        return (
            <div className="container">
                <div className="output">
                    <input type="text" value={this.state.out} readOnly />
                </div>
                <div className="buttons">
                    {store.buttons.map((item, index) => <Button key={index} item={item} onClick={() => this.tapeNumber(item)}/>)}
                    {store.operations.map((item, index) => <Button key={index} item={item} onClick={() => this.tapeOperation(item)}/>)}
                </div>

            </div>
        );
    }
}

export default Calc;

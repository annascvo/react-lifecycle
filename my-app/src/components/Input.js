import PropTypes from 'prop-types'

const Input = ({ className, value, onChange }) => (
    <input className={className} value={value} onChange={onChange} />
);


Input.propTypes = {
    className: PropTypes.string,
    value: PropTypes.string,
    onChange: PropTypes.func
}

export default Input;
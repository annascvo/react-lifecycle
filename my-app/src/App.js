import './App.css';
import React from 'react';
import Calc from "./components/Calc"
import Students from './components/Students';
import MainContainer from './components/MainContainer'

class App extends React.Component {
  render() {
    return (
      <div className="App">
        <Calc />
        <Students />
        <MainContainer/>
      </div>
    );
  }
}

export default App;